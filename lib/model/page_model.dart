
import 'package:json_annotation/json_annotation.dart';

part 'page_model.g.dart';

@JsonSerializable(createToJson: false)
class PageModel{
  PageModel({
    required this.id,
    required this.title,
    this.titlePrefix,
    required this.urn});
  int id;
  String title;
  @JsonKey(name: 'title_prefix')
  String? titlePrefix;
  String urn;


  factory PageModel.fromJson(Map<String, dynamic> json) => _$PageModelFromJson(json);


}