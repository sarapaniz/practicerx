// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'page_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PageModel _$PageModelFromJson(Map<String, dynamic> json) => PageModel(
      id: json['id'] as int,
      title: json['title'] as String,
      titlePrefix: json['title_prefix'] as String?,
      urn: json['urn'] as String,
    );
