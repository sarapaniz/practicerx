import 'dart:async';

import 'package:practicerx/repository/repository.dart';
import 'package:rxdart/rxdart.dart';

import 'package:practicerx/model/page_model.dart';

// import '../model/page_model.dart';

class PageBloc{
  final Repository _repository = Repository();

  final _pageFetcher = PublishSubject<List<PageModel>>();
  // final testStreamController = StreamController<List<Page>>();

  Stream<List<PageModel>> get fetchPages => _pageFetcher.stream;

  fetchAllPage() async{
    List<PageModel> pageData = await _repository.fetchAllPage();
    _pageFetcher.sink.add(pageData);
  }

  void dispose(){
    _pageFetcher.close();
  }


}