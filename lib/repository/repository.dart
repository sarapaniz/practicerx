import 'package:dio/dio.dart';
import 'package:practicerx/data_provider/data_resource.dart';

import 'package:practicerx/model/page_model.dart';

class Repository{

  final DataProvider _dataProvider = DataProvider(Dio(BaseOptions(contentType: "application/json")));

  Future<List<PageModel>> fetchAllPage() => _dataProvider.getPages();

}