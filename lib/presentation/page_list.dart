import 'package:flutter/material.dart';
import 'package:practicerx/bloc/page_bloc.dart';

import 'package:practicerx/model/page_model.dart';

class PageList extends StatelessWidget {
   PageList({Key? key}) : super(key: key);

  final PageBloc _pageBloc = PageBloc();

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text('لیست برنامه ها'),
      ),
      body: StreamBuilder<List<PageModel>>(
        stream: _pageBloc.fetchPages,
        builder: (BuildContext context,AsyncSnapshot<List<PageModel>>? snapshot) {
          var data = snapshot?.data;
          var lenght = snapshot?.data?.length;

          if(snapshot!.hasError){
            return Text('some Error has accurred');
          }
          else if(snapshot.hasData){
            return ListView.builder(
              itemCount: lenght,
                itemBuilder: (BuildContext context, int index ){
                  if(data != null){
                    return Text(data[index].title);
                  }
                  return Text("null List");
                }

            );
          }
          return const Center(child: CircularProgressIndicator(),);
        }
      )
    );
  }
}
