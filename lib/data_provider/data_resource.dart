import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';

import 'package:practicerx/model/page_model.dart';

part 'data_resource.g.dart';

@RestApi(baseUrl:"http://api.tiwall.com/v2")
abstract class DataProvider{
  factory DataProvider(Dio dio, {String baseUrl}) = _DataProvider;

  @GET("/pages/list?tag=1")
  Future<List<PageModel>> getPages();
}